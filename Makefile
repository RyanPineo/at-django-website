SHELL := /bin/sh
MANAGEPY := arenatournament/manage.py
STATIC := arenatournament/static

runserver:
	sudo $(MANAGEPY) runserver 0.0.0.0:80

test:
	$(MANAGEPY) test --settings=arenatournament.settings.test $(APP)

shell:
	$(MANAGEPY) shell

coffee.compile:
	coffee --compile --output $(STATIC)/js $(STATIC)/coffee

coffee.watch:
	coffee --compile --watch --output $(STATIC)/js $(STATIC)/coffee

compass.compile:
	cd $(STATIC); compass compile

compass.watch:
	cd $(STATIC); compass watch
