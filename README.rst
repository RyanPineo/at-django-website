================================
Arena Tournament Django Website
================================
This document will outline information about how this website works and how to get it running.

Development Deployment
=======================

Mac & Linux
------------
Lucky you, deployment is mostly automatic.

1. Install Ansible_, a python automation tool.  Or just ``pip install ansible``.

2. Install Vagrant_, a development environment tool.  For this to work you will also need VirtualBox_.

3. Make a file called ``dbinfo.json`` in the repository root and fill in your information. Example:

    .. sourcecode:: json

        {
            "username": "YOUR_DB_USERNAME",
            "password": "YOUR_DB_PASSWORD"
        }

4. Run ``vagrant up`` and wait a few minutes while it sets everything up.

5. Run ``vagrant ssh`` to ssh in to your new VM and cd to /vagrant.

6. Compile the sass files.  ``make sass.compile``

7. Compile the coffee files.  ``make coffee.compile``

8. Start the server.  ``make runserver``

9. Visit the url in your browser: 192.168.42.12 and you're done.

.. _Ansible: http://docs.ansible.com/intro_installation.html
.. _Vagrant: https://www.vagrantup.com/
.. _VirtualBox: https://www.virtualbox.org/


Windows
--------
Ugh good luck.
