$ ->
    $('.featured-streams a').click(->
        $('.featured-img').attr('src', $(this).data('img'))
        $(".featured-stream-selected a").attr("href", $(this).data("link"))
        $(".featured").removeClass("selected")
        $(this).find(".featured").addClass("selected")
        return false
    )

    $('.featured-streams').mCustomScrollbar()
