# show/hide the login box.
$('.js-login').click ->
    if $('#user-meta-login').is ':hidden'
        $('#user-meta-login').slideDown('fast').show
    else
        $('#user-meta-login').hide('fast')


# Expand the navbar leafs.  I don't even comprehend how this works.
$('#nav ul.menu li a').mouseenter ->
    $(this).parent().find('ul.menu').stop(true, true).slideDown('fast').show
    $(this).parent().mouseleave ->
        $(this).parent().find('ul.menu').stop(true, true).slideUp('fast')
