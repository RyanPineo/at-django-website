from unipath import FSPath as Path

### Utilities

# Django project base.
BASE = Path(__file__).absolute().ancestor(3)

REPO_BASE = BASE.ancestor(1)


### Applications

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'widget_tweaks',
]


ARENATOURNAMENT_APPS = [
    'armory',
    'dbc',
    'forum',
    'realmd',
    'web',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + ARENATOURNAMENT_APPS


### Middleware

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'arenatournament.urls'

WSGI_APPLICATION = 'arenatournament.wsgi.application'


### Date & Time & Language

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


### Static & Media & Templates

STATICFILES_DIRS = [BASE.child('static')]

STATIC_URL = '/static/'

TEMPLATE_DIRS = [BASE.child('templates')]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',

    'realmd.context_processors.login_form',
    'realmd.context_processors.form_require_star',
)


### Database

DATABASE_ROUTERS = ['realmd.router.RealmdRouter', 'forum.router.ForumRouter',
                    'armory.router.ArmoryRouter']


### Auth

AUTH_USER_MODEL = 'realmd.Account'
