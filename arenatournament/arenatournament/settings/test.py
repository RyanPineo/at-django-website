from .base import *

SECRET_KEY = 'blah'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'testdb'
    }
}
DATABASE_ROUTERS = []

TEST_RUNNER = 'utils.test.ManagedModelRunner'
