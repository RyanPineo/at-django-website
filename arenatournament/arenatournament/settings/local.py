import json

from .base import *

with open(REPO_BASE.child('dbinfo.json')) as handle:
    dbinfo = json.load(handle)

SECRET_KEY = 'foobar'

DEBUG = True

TEMPLATE_DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar',
    'template_timings_panel'
]

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',

    'template_timings_panel.panels.TemplateTimings.TemplateTimings',
]

INTERNAL_IPS = ['192.168.42.1']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'NAME': 'atsite_old_web',
        'ENGINE': 'django.db.backends.mysql',
        'USER': dbinfo['username'],
        'PASSWORD': dbinfo['password'],
        'HOST': '5.135.134.58'
    },
    'realmd': {
        'NAME': 'atsite4_realmd',
        'ENGINE': 'django.db.backends.mysql',
        'USER': dbinfo['username'],
        'PASSWORD': dbinfo['password'],
        'HOST': '5.135.134.58'
    },
    'forum': {
        'NAME': 'atsite_phpbb',
        'ENGINE': 'django.db.backends.mysql',
        'USER': dbinfo['username'],
        'PASSWORD': dbinfo['password'],
        'HOST': '5.135.134.58'
    },
    'wotlk_char': {
        'NAME': 'wotlk_characters_main',
        'ENGINE': 'django.db.backends.mysql',
        'USER': dbinfo['username'],
        'PASSWORD': dbinfo['password'],
        'HOST': '5.135.134.58'
    }
}
