from django.db import models


class Stream(models.Model):
    user = models.ForeignKey('User', primary_key=True, db_column='id')
    approved = models.BooleanField()
    featured = models.BooleanField()
    site = models.CharField(max_length=50)
    stream = models.CharField(max_length=50)
    online = models.BooleanField()
    viewers = models.IntegerField()
    img_small = models.CharField(max_length=200)
    img_large = models.CharField(max_length=200)
    own3d_channel = models.CharField(max_length=50)
    touchtime = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'streams'

    def small_preview_img(self):
        return self.preview_img(150, 113)

    def med_preview_img(self):
        return self.preview_img(470, 250)

    def big_preview_img(self):
        pass

    def preview_img(self, width, height):
        return\
            'http://static-cdn.jtvnw.net/previews-ttv/live_user_{}-{}x{}.jpg'\
            .format(self.stream, width, height)


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    forumname = models.TextField(blank=True)
    forumchar = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'users'
