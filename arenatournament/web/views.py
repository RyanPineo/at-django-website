from django.shortcuts import render

from .models import Stream
from armory.models import Character
from forum.models import PhpbbTopic


def index(request):
    print("IP Address for debug-toolbar: " + request.META['REMOTE_ADDR'])
    topics = PhpbbTopic.objects.select_related('topic_first_post').\
        filter(forum_id=9).order_by('-topic_time')[:5]
    return render(request, 'index.html', {'topics': topics})


def streams(request):
    streams = Stream.objects.select_related('user').\
        filter(approved=True, online=True).\
        order_by('-viewers')

    char_ids = [stream.user.forumchar for stream in streams]
    characters = {c.pk: c for c in Character.objects.
                  filter(pk__in=char_ids).
                  prefetch_related('characterarenastat_set')}

    for stream in streams:
        stream.character = characters[stream.user.forumchar]

    regular_streams = [stream for stream in streams if not stream.featured]
    featured_streams = [stream for stream in streams if stream.featured]

    return render(request, 'web/streams.html',
                  {'regular_streams': regular_streams,
                   'featured_streams': featured_streams})
