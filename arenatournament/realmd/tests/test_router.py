from django.test import TestCase

from ..models import Account
from ..router import RealmdRouter


class RealmdRouterTest(TestCase):
    def setUp(self):
        self.router = RealmdRouter()

    def test_db_for_read_realmd_model(self):
        self.assertEqual('realmd', self.router.db_for_read(Account))

    def test_db_for_write_realmd_model(self):
        self.assertEqual('realmd', self.router.db_for_write(Account))

    def test_allow_relation_two_realmd_models(self):
        self.assertTrue(self.router.allow_relation(Account, Account))
