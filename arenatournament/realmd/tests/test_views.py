from utils.test import TestCase
from ..models import Account


def make_account(username='john', email='john@gmail.com', password='password'):
    account = Account()
    account.username = username
    account.email = email
    account.set_password(password)
    account.save()


class RegisterTest(TestCase):
    def test_page_exists(self):
        r = self.client.get('register')
        self.assertEqual(200, r.status_code)

    def test_page_loads(self):
        r = self.client.get('register')
        self.assertIn('Register', r.content)

    def register(self, username, email, password, conf_password):
        r = self.client.post('register', {
            'username': username, 'email': email, 'password': password,
            'conf_password': conf_password})
        return r

    def test_username_unique(self):
        make_account()
        r = self.register('john', 'asd@gmail.com', 'password', 'password')
        self.assertFormError(r, 'form', 'username',
                             'Account with this Username already exists.')

    def test_passwords_must_match(self):
        r = self.register('john', 'asd@gmail.com', 'password', 'notpassword')
        self.assertFormError(r, 'form', 'conf_password',
                             'Your passwords do not match.')

    def test_success_creates_account(self):
        self.register('john', 'asd@gmail.com', 'password', 'password')
        self.assertEqual(1, len(Account.objects.all()))
