from django import forms
from django.contrib.auth.forms import AuthenticationForm

from .models import Account


class RegisterForm(forms.ModelForm):
    conf_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Account
        fields = ['username', 'email', 'password', 'conf_password']
        widgets = dict(password=forms.PasswordInput)

    def clean(self):
        """Validate that the password and con_password match."""
        cleaned_data = super(RegisterForm, self).clean()
        password = cleaned_data.get('password')
        conf_password = cleaned_data.get('conf_password')

        if password and conf_password and password != conf_password:
            msg = 'Your passwords do not match.'

            self._errors['conf_password'] = self.error_class([msg])

            # These fields are no longer valid. Remove them from the
            # cleaned data.
            del cleaned_data['password']
            del cleaned_data['conf_password']

        # Always return the full collection of cleaned data.
        return cleaned_data

    def save(self, commit=False):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(user.password)
        user.save()
        return user


class LoginForm(AuthenticationForm):
    error_messages = {
        'invalid_login': 'Invalid Username or Password',
        'inactive': 'This account is banned.'
    }
