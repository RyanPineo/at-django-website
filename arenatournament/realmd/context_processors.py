from .forms import LoginForm


def login_form(request):
    return {'login_form': LoginForm()}


def form_require_star(request):
    return {
        'form_req_star':
        '<span class="form-required" title="This field is required.">*</span>'}
