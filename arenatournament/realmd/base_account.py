from hashlib import sha1

from django.contrib.auth.models import UserManager
from django.db import models
from django.utils import timezone


class BaseAccount(models.Model):
    id = models.AutoField(primary_key=True)
    sessionkey = models.TextField(blank=True)
    v = models.TextField(blank=True)
    s = models.TextField(blank=True)
    joindate = models.DateTimeField(default=timezone.now())
    last_ip = models.CharField(max_length=30)
    failed_logins = models.IntegerField(default=0)
    last_login = models.DateTimeField(default=timezone.now)
    locale = models.IntegerField(default=0)
    old_email = models.TextField(blank=True)
    ng_account_id = models.IntegerField(db_column='NG_accountid',
                                        blank=True, null=True)
    ng_username = models.CharField(db_column='NG_username',
                                   max_length=32, blank=True)
    ng_sha_pass_hash = models.CharField(db_column='NG_sha_pass_hash',
                                        max_length=40, blank=True)

    objects = UserManager()

    is_active = True
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        abstract = True

    def get_username(self):
        "Return the identifying username for this User"
        return getattr(self, self.USERNAME_FIELD)

    def __str__(self):
        return self.get_username()

    def natural_key(self):
        return (self.get_username(),)

    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        self.password = self.hash_password(raw_password)
        # Need to reset these so you can log ingame.
        self.v = ''
        self.s = ''
        self.sessionkey = ''

    def hash_password(self, raw_password):
        hash = sha1('%s:%s' % (self.username.upper(), raw_password.upper()))
        return hash.hexdigest().upper()

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        return self.hash_password(raw_password) == self.password.upper()

    def set_unusable_password(self):
        # Sets a value that will never be a valid hash
        self.password = ''

    def has_usable_password(self):
        return self.password != ''
