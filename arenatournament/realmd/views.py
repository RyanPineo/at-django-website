from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from django.utils.http import is_safe_url

from .forms import RegisterForm, LoginForm


class Register(CreateView):
    template_name = 'realmd/register.html'
    form_class = RegisterForm
    success_url = '/'


def login(request):
    if request.method != "POST":
        return HttpResponseRedirect('/')

    redirect_to = request.REQUEST.get('next', '/')

    form = LoginForm(request, data=request.POST)
    if form.is_valid():

        # Ensure the user-originating redirection url is safe.
        if not is_safe_url(url=redirect_to, host=request.get_host()):
            redirect_to = '/'

        # Okay, security check complete. Log the user in.
        auth_login(request, form.get_user())

    return HttpResponseRedirect(redirect_to)
