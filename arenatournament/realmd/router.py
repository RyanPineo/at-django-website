class RealmdRouter(object):
    """
    A router to control all database operations on models in the
    account application.
    """
    def db_for_read(self, model, **hints):
        """Attempts to read realmd app go to realmd db."""
        app_label = model._meta.app_label
        if app_label == 'realmd':
            return 'realmd'
        return None

    def db_for_write(self, model, **hints):
        """Attempts to write realmd app go to realmdb db."""
        app_label = model._meta.app_label
        if app_label == 'realmd':
            return 'realmd'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """Allow relations between models in the realmd app."""
        app_label_1 = obj1._meta.app_label
        app_label_2 = obj2._meta.app_label
        if app_label_1 == 'realmd' and app_label_2 == 'realmd':
            return True
        return None
