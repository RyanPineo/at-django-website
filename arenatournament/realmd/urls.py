from django.conf.urls import url
from django.contrib.auth.views import logout

from .views import Register, login

urlpatterns = [
    url(r'^register$', Register.as_view(), name='register'),

    url(r'^login$', login, name='login'),

    url(r'^logout$', logout, {'next_page': 'index'}, name='logout'),
]
