from __future__ import unicode_literals

from django.db import models
from django.core import validators

from .base_account import BaseAccount


class Account(BaseAccount):
    username = models.CharField(unique=True, max_length=32)
    password = models.CharField(
        max_length=40, db_column='sha_pass_hash',
        validators=[validators.MinLengthValidator(6),
                    validators.MaxLengthValidator(16)]
    )
    gmlevel = models.IntegerField(default=0)
    email = models.EmailField(blank=True)
    locked = models.IntegerField(default=0)
    online = models.BooleanField(default=0)
    expansion = models.IntegerField(default=3)
    mutetime = models.BigIntegerField(default=0)
    recruiter = models.IntegerField(default=0)
    operating_system = models.IntegerField(db_column='operatingSystem',
                                           default=0)
    force_password_change = models.IntegerField(
        db_column='forcePasswordChange', blank=True, null=True)
    newsletter = models.IntegerField(blank=True, null=True)
    raf_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'account'
