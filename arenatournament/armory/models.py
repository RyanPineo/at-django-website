from django.db import models

from dbc import CharClass


class ArenaTeam(models.Model):
    id = models.AutoField(primary_key=True, db_column='arenateamid')
    name = models.CharField(max_length=255)
    captain = models.ForeignKey('Character', db_column='captainguid')
    type = models.IntegerField()
    background_color = models.IntegerField(db_column='BackgroundColor')
    emblem_style = models.IntegerField(db_column='EmblemStyle')
    emblem_color = models.IntegerField(db_column='EmblemColor')
    border_style = models.IntegerField(db_column='BorderStyle')
    border_color = models.IntegerField(db_column='BorderColor')

    class Meta:
        managed = False
        db_table = 'arena_team'

    def bracket(self):
        return '{1}v{1}'.format(self.type)


class ArenaTeamMember(models.Model):
    # The real primary key is a combination of arenateamid and guid.
    team = models.ForeignKey('ArenaTeam', primary_key=True,
                             db_column='arenateamid')
    character = models.ForeignKey('Character', db_column='guid')
    week_played = models.IntegerField(db_column='played_week')
    week_wins = models.IntegerField(db_column='wons_week')
    season_played = models.IntegerField(db_column='played_season')
    season_wins = models.IntegerField(db_column='wons_season')
    personal_rating = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'arena_team_member'

    def week_loses(self):
        return self.week_played - self.week_wins

    def season_loses(self):
        return self.season_played - self.season_wins


class ArenaTeamStat(models.Model):
    team = models.ForeignKey('ArenaTeam', primary_key=True,
                             db_column='arenateamid')
    rating = models.IntegerField()
    week_played = models.IntegerField(db_column='games')
    week_wins = models.IntegerField(db_column='wins')
    season_played = models.IntegerField(db_column='played')
    season_wins = models.IntegerField(db_column='wins2')
    rank = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'arena_team_stats'

    def week_loses(self):
        return self.week_played - self.week_wins

    def season_loses(self):
        return self.season_played - self.season_wins


class Character(models.Model):
    id = models.AutoField(primary_key=True, db_column='guid')
    account = models.IntegerField()
    name = models.CharField(max_length=13)
    race_id = models.IntegerField(db_column='race')
    class_id = models.IntegerField(db_column='class')
    gender = models.IntegerField()
    player_bytes = models.IntegerField(db_column='playerBytes')
    player_bytes2 = models.IntegerField(db_column='playerBytes2')
    player_flags = models.IntegerField(db_column='playerFlags')
    position_x = models.FloatField()
    position_y = models.FloatField()
    position_z = models.FloatField()
    map = models.IntegerField()
    instance_id = models.IntegerField()
    instance_mode_mask = models.IntegerField()
    orientation = models.FloatField()
    online = models.IntegerField()
    totaltime = models.IntegerField()
    logout_time = models.BigIntegerField()
    trans_x = models.FloatField()
    trans_y = models.FloatField()
    trans_z = models.FloatField()
    trans_o = models.FloatField()
    transguid = models.BigIntegerField()
    extra_flags = models.IntegerField()
    at_login = models.IntegerField()
    zone = models.IntegerField()
    death_expire_time = models.BigIntegerField()
    arena_points = models.IntegerField(db_column='arenaPoints')
    total_honor_points = models.IntegerField(db_column='totalHonorPoints')
    today_honor_points = models.IntegerField(db_column='todayHonorPoints')
    yesterday_honor_points = models.IntegerField(
        db_column='yesterdayHonorPoints')
    total_kills = models.IntegerField(db_column='totalKills')
    today_kills = models.IntegerField(db_column='todayKills')
    yesterday_kills = models.IntegerField(db_column='yesterdayKills')
    chosen_title = models.IntegerField(db_column='chosenTitle')
    known_currencies = models.BigIntegerField(db_column='knownCurrencies')
    watched_faction = models.BigIntegerField(db_column='watchedFaction')
    drunk = models.IntegerField()
    health = models.IntegerField()
    power1 = models.IntegerField()
    power2 = models.IntegerField()
    power3 = models.IntegerField()
    power4 = models.IntegerField()
    power5 = models.IntegerField()
    power6 = models.IntegerField()
    power7 = models.IntegerField()
    speccount = models.IntegerField()
    activespec = models.IntegerField()
    explored_zones = models.TextField(db_column='exploredZones', blank=True)
    equipment_cache = models.TextField(db_column='equipmentCache', blank=True)
    ammo_id = models.IntegerField(db_column='ammoId')
    titles1 = models.IntegerField(blank=True, null=True)
    titles2 = models.IntegerField(blank=True, null=True)
    titles3 = models.IntegerField(blank=True, null=True)
    titles4 = models.IntegerField(blank=True, null=True)
    titles5 = models.IntegerField(blank=True, null=True)
    titles6 = models.IntegerField(blank=True, null=True)
    action_bars = models.IntegerField(db_column='actionBars')
    delete_infos_account = models.IntegerField(
        db_column='deleteInfos_Account', blank=True, null=True)
    delete_infos_name = models.CharField(
        db_column='deleteInfos_Name', max_length=12, blank=True)
    delete_date = models.BigIntegerField(
        db_column='deleteDate', blank=True, null=True)
    latency = models.IntegerField()
    transmog_set = models.IntegerField(db_column='transmogSet')

    class Meta:
        managed = False
        db_table = 'characters'

    def arena_mmr_2v2(self):
        return self.arena_mmr(0)

    def arena_mmr_3v3(self):
        return self.arena_mmr(1)

    def arena_mmr(self, slot):
        for stat in self.characterarenastat_set.all():
            if stat.slot == slot:
                return stat.mmr
        else:
            return 1500

    def arena_mmr_solo(self):
        solo_stat = self.soloqueuestat_set.filter()
        if solo_stat:
            return solo_stat[0].mmr
        return 1500

    def class_name(self):
        return CharClass.get_name(self.class_id)

    def class_icon(self):
        return 'http://wow.zamimg.com/images/wow/icons/medium/class_{}.jpg'.\
            format(self.class_name().lower().replace(' ', ''))


class CharacterArenaStat(models.Model):
    # The real primary key is a combination of guid and slot.
    character = models.ForeignKey('Character',
                                  primary_key=True, db_column='guid')
    slot = models.IntegerField()
    pr = models.IntegerField(db_column='personal_rating')
    mmr = models.IntegerField(db_column='matchmaker_rating')

    class Meta:
        managed = False
        db_table = 'character_arena_stats'


class SoloqueueStat(models.Model):
    character = models.ForeignKey('Character',
                                  primary_key=True, db_column='guid')
    mmr = models.IntegerField()
    games = models.IntegerField()
    wins = models.IntegerField()
    gamesweek = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'soloqueue_stats'
