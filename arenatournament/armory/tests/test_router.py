from django.test import TestCase

from ..models import Character
from ..router import ArmoryRouter


class ForumRouterTest(TestCase):
    def setUp(self):
        self.router = ArmoryRouter()

    def test_db_for_read_forum_model(self):
        self.assertEqual('wotlk_char', self.router.db_for_read(Character))

    def test_db_for_write_forum_model(self):
        self.assertEqual('wotlk_char', self.router.db_for_write(Character))

    def test_allow_relation_two_forum_models(self):
        self.assertTrue(self.router.allow_relation(Character, Character))
