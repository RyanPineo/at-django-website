from django.core.urlresolvers import reverse
from django.test.runner import DiscoverRunner
from django.test import TestCase, Client


class ManagedModelRunner(DiscoverRunner):
    """
    Test runner that automatically makes all unmanaged models in your Django
    project managed for the duration of the test run, so that one doesn't need
    to execute the SQL manually to create them.
    """

    def setup_test_environment(self, *args, **kwargs):
        from django.db.models.loading import get_models
        self.unmanaged_models = [m for m in get_models()
                                 if not m._meta.managed]
        for m in self.unmanaged_models:
            m._meta.managed = True
        super(ManagedModelRunner, self).setup_test_environment(*args,
                                                               **kwargs)

    def teardown_test_environment(self, *args, **kwargs):
        super(ManagedModelRunner, self).teardown_test_environment(*args,
                                                                  **kwargs)
        # reset unmanaged models
        for m in self.unmanaged_models:
            m._meta.managed = False


class NameClient(Client):
    """Allows get/post url to specified by name instead of by path."""
    def get(self, name, *args, **kwargs):
        return super(NameClient, self).post(reverse(name), *args, **kwargs)

    def post(self, name, *args, **kwargs):
        return super(NameClient, self).post(reverse(name), *args, **kwargs)


class TestCase(TestCase):
    client_class = NameClient
