import re


BBCODE_TRANSLATIONS = [
    # Bold & underline & italic.
    (r'\[(b|u|i)\](.*?)\[\/(b|u|i)\]', r'<\1>\2</\1>'),

    # Img / hrefs.
    (r'\[img\](.*?)\[\/img\]', r'<img src="\1" />'),
    (r'\[url\](.*?)\[\/url\]', r'<a href="\1">\1</a>'),
    (r'\[url=(.*?)\](.*?)\[\/url\]', r'<a href="\1">\2</a>'),
    (r'\[email\](.*?)\[\/email\]', r'<a href="mailto:\1">\1</a>'),
    (r'\[email=(.*?)\](.*?)\[\/email\]', r'<a href="mailto:\1">\2</a>'),

    # Font size.
    (r'\[size=(.*?)\](.*?)\[\/size\]',
     r'<span style="font-size: \1%;">\2</span>'),

    # Font color.
    (r'\[color=(.*?)\](.*?)\[\/color\]',
     r'<span style="color: \1;">\2</span>'),

    # Font family.
    (r'\[font=(.*?)\](.*?)\[\/font\]',
     r'<span style="font-family: \1;">\2</span>'),

    # Code.
    (r'\[code\](.*?)\[\/code\]',
     (r'<span style="font-family: Courier;'
      r'display: block; border: 1px solid white;">\1</span>')),

    # Spoiler.
    (r'\[spoiler\](.*?)\[\/spoiler\]',
     (r'<div class="bbcode_spoiler">'
      r'<a class="bbcode_spoiler_toggle" href="#">Spoiler</a>'
      r'<div class="bbcode_spoiler_content">\1</div>'
      r'</div>')),

    # Twitch stream.
    (r'\[twitch\](.*?)\[\/twitch\]',
     (r'<object type="application/x-shockwave-flash" height="433" width="720"'
      r'id="live_embed_player_flash"'
      r'data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=\1"'
      r'bgcolor="#000000">'
      r'<param name="allowFullScreen" value="true" />'
      r'<param name="allowScriptAccess" value="always" />'
      r'<param name="allowNetworking" value="all" />'
      r'<param name="movie"'
      r'value="http://www.twitch.tv/widgets/live_embed_player.swf" />'
      r'<param name="flashvars"'
      r'value="hostname=www.twitch.tv&channel=\1&auto_play=true" />'
      r'</object>')),

    # Twitch chat.
    (r'\[twitchchat\](.*?)\[\/twitchchat\]',
     (r'<iframe frameborder="0" scrolling="no" id="chat_embed"'
      r'src="http://twitch.tv/chat/embed?channel=\1&amp;popout_chat=true"'
      r'height="405" width="720"></iframe>')),

    # List item.
    (r'\[\*\](.*?)\[\/\*\:[a-z]\]', r'<li>\1</li>'),
    (r'\<\/li\>\<br\s\/\>', r'</li>'),

    # Alphabet ordered list.
    (r'\[list=a\](.*?)\[\/list\:[a-z]\]',
     r'<ol style="list-style-type: lower-alpha;">\1</ol>'),

    # Numbered ordered list
    (r'\[list=1\](.*?)\[\/list\:[a-z]\]',
     r'<ol style="list-style-type: decimal;">\1</ol>'),

    # Unordered list
    (r'\[list\](.*?)\[\/list\:[a-z]\]',
     r'<ul>\1</ul>'),

    (r'\<(ol|ul)(.*?)\>\<br\s\/\>', r'<\1\2>'),

]


def parse_bbcode(text):
    for pattern, replacement in BBCODE_TRANSLATIONS:
        text = re.sub(pattern, replacement, text,
                      flags=re.IGNORECASE + re.DOTALL)
    return text
