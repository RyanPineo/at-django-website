from django.test import TestCase

from ..models import PhpbbTopics
from ..router import ForumRouter


class ForumRouterTest(TestCase):
    def setUp(self):
        self.router = ForumRouter()

    def test_db_for_read_forum_model(self):
        self.assertEqual('forum', self.router.db_for_read(PhpbbTopics))

    def test_db_for_write_forum_model(self):
        self.assertEqual('forum', self.router.db_for_write(PhpbbTopics))

    def test_allow_relation_two_forum_models(self):
        self.assertTrue(self.router.allow_relation(PhpbbTopics, PhpbbTopics))
