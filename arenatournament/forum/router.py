class ForumRouter(object):
    """
    A router to control all database operations on models in the
    account application.
    """
    def db_for_read(self, model, **hints):
        """Attempts to read forum app go to forum db."""
        app_label = model._meta.app_label
        if app_label == 'forum':
            return 'forum'
        return None

    def db_for_write(self, model, **hints):
        """Attempts to write forum app go to forum db."""
        app_label = model._meta.app_label
        if app_label == 'forum':
            return 'forum'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """Allow relations between models in the forum app."""
        app_label_1 = obj1._meta.app_label
        app_label_2 = obj2._meta.app_label
        if app_label_1 == 'forum' and app_label_2 == 'forum':
            return True
        return None
