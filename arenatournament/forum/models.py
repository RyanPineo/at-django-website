from datetime import datetime

from django.db import models

from utils.bbcode import parse_bbcode


class PhpbbTopic(models.Model):
    topic_id = models.IntegerField(primary_key=True)
    forum_id = models.IntegerField()
    icon_id = models.IntegerField()
    topic_attachment = models.IntegerField()
    topic_approved = models.IntegerField()
    topic_reported = models.IntegerField()
    topic_title = models.CharField(max_length=255)
    topic_poster = models.IntegerField()
    topic_time = models.IntegerField()
    topic_time_limit = models.IntegerField()
    topic_views = models.IntegerField()
    topic_replies = models.IntegerField()
    topic_replies_real = models.IntegerField()
    topic_status = models.IntegerField()
    topic_type = models.IntegerField()
    topic_first_post = models.ForeignKey('PhpbbPost',
                                         db_column='topic_first_post_id')
    topic_first_poster_name = models.CharField(max_length=255)
    topic_first_poster_colour = models.CharField(max_length=6)
    topic_last_post_id = models.IntegerField()
    topic_last_poster_id = models.IntegerField()
    topic_last_poster_name = models.CharField(max_length=255)
    topic_last_poster_colour = models.CharField(max_length=6)
    topic_last_post_subject = models.CharField(max_length=255)
    topic_last_post_time = models.IntegerField()
    topic_last_view_time = models.IntegerField()
    topic_moved_id = models.IntegerField()
    topic_bumped = models.IntegerField()
    topic_bumper = models.IntegerField()
    poll_title = models.CharField(max_length=255)
    poll_start = models.IntegerField()
    poll_length = models.IntegerField()
    poll_max_options = models.IntegerField()
    poll_last_vote = models.IntegerField()
    poll_vote_change = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'phpbb_topics'

    @property
    def topic_datetime(self):
        return datetime.fromtimestamp(self.topic_time)


class PhpbbPost(models.Model):
    post_id = models.IntegerField(primary_key=True)
    topic_id = models.IntegerField()
    forum_id = models.IntegerField()
    poster_id = models.IntegerField()
    icon_id = models.IntegerField()
    poster_ip = models.CharField(max_length=40)
    post_time = models.IntegerField()
    post_approved = models.IntegerField()
    post_reported = models.IntegerField()
    enable_bbcode = models.IntegerField()
    enable_smilies = models.IntegerField()
    enable_magic_url = models.IntegerField()
    enable_sig = models.IntegerField()
    post_username = models.CharField(max_length=255)
    post_subject = models.CharField(max_length=255)
    post_text = models.TextField()
    post_checksum = models.CharField(max_length=32)
    post_attachment = models.IntegerField()
    bbcode_bitfield = models.CharField(max_length=255)
    bbcode_uid = models.CharField(max_length=8)
    post_postcount = models.IntegerField()
    post_edit_time = models.IntegerField()
    post_edit_reason = models.CharField(max_length=255)
    post_edit_user = models.IntegerField()
    post_edit_count = models.IntegerField()
    post_edit_locked = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'phpbb_posts'

    def parsed_text(self):
        text = self.post_text.replace(':%s' % self.bbcode_uid, '')
        return parse_bbcode(text)
